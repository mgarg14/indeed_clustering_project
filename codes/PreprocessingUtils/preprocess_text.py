#!/usr/bin/env python
# coding: utf-8

# #### Goal: Aims to clean (remove stopwords, punctuations and unused character) and preprocess (stem and lemmatize) the texts for further clustering and TF-IDF scoring

# In[34]:


import re
import nltk
#nltk.download('stopwords')
#nltk.download('wordnet')
#nltk.download('punkt')
# uncomment the above lines when you don't have 'stopwords'/'wordnet/punkt' installed (needed for preprocessing)


# In[35]:


from nltk.corpus import stopwords  
from nltk.stem import WordNetLemmatizer, PorterStemmer, LancasterStemmer, SnowballStemmer
from nltk.tokenize import RegexpTokenizer, sent_tokenize
from gensim.models.phrases import Phrases, Phraser


# In[52]:


class Preprocess(object):
    def __init__(self, enable_stemming=False,stemmer='Snowball',regex=r'[a-zA-Z\']+'):
        self.enable_stemming = enable_stemming
        self.stemmer = stemmer
        self.stopwords = set(stopwords.words('english'))
        self.tokenizer = RegexpTokenizer(regex)
        
        #initialization step for lemmatizer and stemmers
        if not self.initialize_lemma_stemmer():
            raise RuntimeError("Error while initializing lemmatizer/stemmer")
            
    #TODO: make use of this function (if required)
    def stop_words(self):
        """ Modify this function to add more criteria to stopwords
        """
        self.stopwords = self.stop_words.union(self.punc)
    
    def initialize_lemma_stemmer(self):
        ''' This function performs 
        1. lemmatization 
        2. stemming: (if enable_stemming is set to true during initialization) based on the type of stemmer to use. 
        
        Args:
            self: object of self class
        Returns:
            True : if initialization successful, else raises error
        '''
        # mandating use of lemmatizer
        self.lemmatizer = WordNetLemmatizer()
        #TODO: can add more stemmers in here. 
        if self.enable_stemming:
            if self.stemmer == 'Porter':
                self.stemming = PorterStemmer()
            elif self.stemmer == 'Lancaster':
                self.stemming = LancasterStemmer()
            elif self.stemmer == 'Snowball':
                self.stemming = SnowballStemmer("english")  
            else:
                #TODO: log this error/report or define a default stemmer
                raise RuntimeError("User needs to provide a suitable stemmer")
        return True
    
    def make_bigrams(self, doc, min_count=1, threshold=2):
        """ Find the bigrams for text (usually an unprocessed paragraph). This function 
        breaks paragraphs into sentences and returns list of processed tokens which needs to be further 
        cleaned accordingly ex. stopwords, lemmatized, stemmed etc. 
        Make sure to treat the bigram words as single word during processing
        Args:
        doc (list) : list of sentence tokens (can be raw or processed) created from a paragraph. Ex : "tom is a young boy. he is playing football" -> [["tom","young","boy"],["playing","football"]]
        min_count (float) :  Ignore all words and bigrams with total collected count lower than this value.
        threshold (float) : Represent a score threshold for forming the phrases (higher means fewer phrases). A phrase of words a followed by b is accepted if the score of the phrase is greater than threshold
        """
        phrases = Phrases(doc, min_count=min_count, threshold=threshold) 
        bigrams = Phraser(phrases) 
        return bigrams

    def clean_text(self,text,enable_bigrams=True,min_bigram_count=1,bigram_threshold=2):
        ''' This function performs the following operation in this order: 
         sentence tokenization -> word tokenization -> remove stop_words -> lemmatize -> stemming (optional) -> form bigrams (optional)
        Args:
            text: str from a document
            enable_bigrams : (bool) to enable use of bigrams while processing, default = True
            The two parameters below will be used only when `enable_bigrams` is set to true
            min_bigram_count (float) :  Ignore all words and bigrams with total collected count lower than this value.
            bigram_threshold (float) : Represent a score threshold for forming the phrases (higher means fewer phrases). A phrase of words a followed by b is accepted if the score of the phrase is greater than threshold   
        Returns:
            lemmas : list of processed token
        '''
        sentences = sent_tokenize(text.lower())
        lemma_list=[]
        for sents in sentences: 
            if(self.enable_stemming):
                lemmas = [self.stemming.stem(self.lemmatizer.lemmatize(w, pos='v')) for w in self.tokenizer.tokenize(sents.lower()) if w not in self.stopwords]
                # lemmatize groups semantics, here pos='v' will take care of verbs
                # for example, 'are', 'were' and 'is' should all be categorized under 'be'
            else:
                lemmas = [self.lemmatizer.lemmatize(w, pos='v') for w in self.tokenizer.tokenize(sents.lower()) if w not in self.stopwords]
            lemma_list.append(lemmas)
        if(enable_bigrams) : 
            # if bigrams are enabled
            bigram_model = self.make_bigrams(lemma_list,min_count= min_bigram_count,threshold=bigram_threshold)
            return [bigram_model[words] for words in lemma_list]
        else:
            return lemma_list


# In[59]:


#s = "job title, keywords, or company city or province Software Developer Intern Synopsys - Calgary, AB Our team is looking for a Java Software Developer intern (for 12 months) to help us develop the next generation of cloud services to support the Synopsys Software Integrity Platform(SWIP). Learn about microservices, scalability, inter-service interactions, event stores, and other distributed computing concepts. SWIP is being built from the ground up to manage results from all of the software analysis tools that Synopsys develops and is designed to scale to millions of issues using a microservices architecture deployed on public cloud infrastructure such as Amazon Web Services. The ideal candidate is someone who: has outstanding analytical skills and strong problem solving abilities strives to produce quality code that isn't finished until it is tested thrives in a fast-paced team environment Skill Requirements: Must have: Completed 3 years of university level Computer Science, Software Engineering, Computer Engineering or equivalent (before starting your internship) Excellent problem solving skills and enjoy challenging work Experience with Java development and object oriented design Bonus experience: Experience using command line interfaces Experience with Mac/Windows/Linux platforms Experience with Go development Personal Qualities: Strong English communicator (written and verbal) Willing to pitch in wherever necessary Strong interest in learning and growing You care (about others, about doing a good job, about details) Motivated by quality, excellence, and results Education Requirements: Completed 3 years of university level Computer Science, Software Engineering, Computer Engineering, or equivalent (before starting your internship) Synopsys technology is at the heart of innovations that are changing the way we live and work. The Internet of Things. Autonomous cars. Wearables. Smart medical devices. Secure financial services. Machine learning and computer vision. These breakthroughs are ushering in the era of Smart, Secure Everything?where devices are getting smarter, everything�s connected, and everything must be secure. Powering this new era of technology are advanced silicon chips, which are made even smarter by the remarkable software that drives them. Synopsys is at the forefront of Smart, Secure Everything with the world�s most advanced tools for silicon chip design, verification, IP integration, and application security testing. Our technology helps customers innovate from Silicon to Software, so they can deliver Smart, Secure Everything. Every business runs on software, and defects in software create risk. We�ve curated the most powerful products and services to create one comprehensive platform that enables our customers to detect and remediate defects across their entire software development life cycle�eliminating risk before it puts them at risk. We believe that software can be free of security and quality defects the first time it is released, improving productivity, and reducing costs. Synopsys Canada ULC values the diversity of our workforce. We are committed to provide access & opportunity to individuals with disabilities and will provide reasonable accommodation to individuals throughout the recruitment and employment process. Should you require an accommodation, please contact hr-help-canada@synopsys.com. Synopsys - 30+ days ago Other jobs you may like RS Energy Group Calgary, AB 6 hours ago Wedge Networks Calgary, AB Easily apply 10 hours ago IHS Markit Calgary, AB 5 days ago Get the top job search app"
#p = Preprocess()


# In[20]:


#temp1 = p.clean_text(s,enable_bigrams=True)
#temp


# In[ ]:




