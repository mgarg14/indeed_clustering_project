#!/usr/bin/env python
# coding: utf-8

# ## Job profile segementation 
# #### Goal : As an initial step, we aim to separate out the job descriptions for each job profile from the dataset collected from indeed. Check the description of original dataset here. [Insert link for description] 

# In[1]:


import pandas as pd
import os


# In[2]:


class segmentProfiles(object):
    """ class object to create files based on job titles.
    """
    def __init__(self,opt_folder="../data/",segment=True):
        '''
        initialization function to segment profiles and save the result in opt_folder (if it does not exist, create one)
        Args:
        opt_folder (str) : location to save the output
        segment (bool) : If True, the text is segmented based on the profiles , 
                         If False, merge the text together.  
        '''
        self.profile_dict = {}
        self.output_folder = opt_folder
        if not os.path.exists(opt_folder):
            os.mkdir(opt_folder)
            #Folder created
        self.segment = segment 
        if not self.segment:
            self.name = 'alltext_unsegmented'
            
    def get_profile_dict(self):
        return self.profile_dict
    
    #TODO : remove this function and use the api from preprocess class
    def get_clean_string(self,string):   
        # change to lowercase, remove stop words
        words = string.lower().split()  #cretae tokens
        #words = [wrds for wrds in words if wrds not in self.stop_words]
        # remove puctuations
        return " ".join(words)
    
    def set_profile_text(self,profile,text):
        """ Concatenate clean texts as string for each profile
        """
        dictionary = self.get_profile_dict()
        if not self.segment:
            profile = self.name
        orig_text = dictionary.get(profile,"")
        dictionary[profile]= "{}.{}".format(orig_text,text).strip()
    
    def set_profile_lists(self,profile,lis):
        """ Concatenate clean texts as list of lists for each profile
        Args:
        profile : (str)  profile name
        lis : (list) list of text to append 
        """
        dictionary = self.get_profile_dict()
        if not self.segment:
            profile = self.name
        orig_lis = dictionary.get(profile,[])
        orig_lis.append(lis)
        dictionary[profile] = orig_lis
        
        
    def get_job_titles(self,elements,column,delimiter=';'):
        """ Function to separate out the job titles from the dataset , using `delimiter` as separator
        """
        # TODO: titles can potentially be grouped together for similar roles eg. (data science -> data scientist)
        titles = [els.strip() for els in elements[column].split(delimiter) if els != '']
        return titles
    
    def get_profile_info(self,profile):
        """ getter function for profile_dictionary
        if profile='all' , return the complete dictionary
        """
        if(profile=='all'):
            return self.profile_dict
        else:
            if not self.segment:
                return self.profile_dict.get(self.name,"")
            else:
                return self.profile_dict.get(profile,"")
    
    def create_summary_profiles(self,profiles='all'):
        ''' create text files for each profile (if profile=='all'),
            else only create file for specified profile. 
        '''
        if profiles=='all':
            ls = list(self.profile_dict.keys())
        else:
            ls = [profiles]
        for profile in ls:
            with open(self.output_folder+profile,'w',encoding='utf8') as f:
                f.write(self.profile_dict.get(profile))
            f.close()


# In[3]:


'''
if __name__ == '__main__':
    source_location="../../data/indeed_ca_dataset_sample.csv"
    segment_profiles = segmentProfiles(opt_folder="../../data/opt_1/",segment=False)
    df = pd.read_csv(source_location,usecols=['keyword','summary'])
    #prep = Preprocess(enable_stemming=False,stemmer='Snowball')
    for i,rows in df.iterrows():
            summary = segment_profiles.get_clean_string(str(rows['summary']))
            #summary = " ".join(prep.clean_text(str(rows['summary'])))
            titles = segment_profiles.get_job_titles(rows,'keyword',delimiter=";")
            for title in titles: 
                segment_profiles.set_profile_text(title,summary)
    segment_profiles.create_summary_profiles('all')
'''

