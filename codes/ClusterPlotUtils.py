#!/usr/bin/env python
# coding: utf-8


import matplotlib.pyplot as plt
import os
import pandas as pd
import pickle
import random
from sklearn.cluster import KMeans
from sklearn.preprocessing import normalize
from sklearn.manifold import TSNE
from sklearn.decomposition import PCA
from sklearn.metrics import silhouette_score
#from wordcloud import WordCloud


default_words_for_clustering = ['python','ml','css','html','javascript']


def get_labels_in_clusters(cluster_labels, vocab, cluster_dc={}):
        """ Returns clusterwise labels formed from Kmeans clustering
        Args:
        clusters: (list) array of size same as vocab, containing the labels of cluster it belogs to for each element.
        vocab : (list) array of words used for creating KMeans clusters
        cluster_dc : (dict) dictionary with cluster labels as keys, and list of words belonging to each label.
        """
        for l, v in zip(cluster_labels, vocab):
                vlis = cluster_dc.get(l, [])
                vlis.append(v)
                cluster_dc[l] = vlis
        return cluster_dc


def create_kmeans_cluster(X, vocab, normalized=True, n_clusters=5,reduction_method='TSNE' ):
        """ Initializes Kmeans Model and fits to the reduced data using (TSNE/PCA)
        Args:
            X (2d array): Vectors belonging to each word in our vocabulary
            vocab (list): vocabulary to use
            n_clusters (int) default number of cluster to create
            normalize (bool) :True for using normalized data to fit, else False
            reduction_method (str): Specifies which dimensionality reduction method to use (TSNE and PCA supported)
        Returns:
            km_model: (object) of sklearn.cluster.Kmeans
            y_kmeans : (list) predicted cluster index for each sample.
            X (2d array) : Transformed vector , reduced to nX2(n_components) dimensions
        """
        if (reduction_method == 'TSNE'):
                model_dim_reduce = TSNE(n_components=2)
        else:
                model_dim_reduce = PCA(n_components=2)

        if (normalized):
                X = model_dim_reduce.fit_transform(normalize(X))
        else:
                X = model_dim_reduce.fit_transform(X)
        km_model = KMeans(n_clusters=n_clusters)
        y_kmeans = km_model.fit_predict(X)
        return km_model, y_kmeans, X


def validate_clusters_silhoutte(obj, vocab=[], n_cluster=5, normalized=False,reduction_method='TSNE', scores={}):
        """ Validate the clusters created using Silhoutte score .
            Args:
                obj (object) of word2vec class
                vocab (list) vocabulary to use
                n_cluster (int) endpoint of range of clusters to validate [0-n_clusters]
                normalize (bool) :True for using normalized data to fit, else False
                reduction_method (str): Specifies which dimensionality reduction method to use
            return:
             scores (dict): cluster number as element and its silhoutte score calculated
            """
        if (len(vocab) == 0):  # If no vocabulary is defined consider all of the words
                X = obj.model[obj.model.wv.vocab]
        else:
                X = obj.model[vocab]
        for n in range(2, n_cluster):
                km_model, y_kmeans, X = create_kmeans_cluster(X, vocab, normalized=normalized, n_clusters=n,
                                                              reduction_method=reduction_method)
                score = silhouette_score(X, y_kmeans, metric='cosine')
                scores[n] = score
        return scores

def cluster_plot(obj, vocab=[], n_clusters=5, opt_location="../plots/", reduction_method='TSNE'):
        """ creates cluster scatter plots from Word2Vec object using K-Means
        Args:
            obj (object) of word2vec class
            vocab (list) vocabulary to use
            n_clusters (int) default number of cluster to create
        """

        if (len(vocab) == 0):  # If no vocabulary is defined consider all of the words
                X = obj.model[obj.model.wv.vocab]
                words = obj.model.wv.vocab.keys()
        else:
                X = obj.model[vocab]
                words = vocab
        km_model, y_kmeans, X = create_kmeans_cluster(X, vocab, normalized=True, n_clusters=n_clusters,
                                                      reduction_method=reduction_method)
        plt.figure(figsize=(10, 10), dpi=320)
        plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, s=50, cmap='viridis')
        for i, coords in enumerate(X):
                plt.annotate(words[i], xy=(coords[0], coords[1]))
        centers = km_model.cluster_centers_
        plt.scatter(centers[:, 0], centers[:, 1], c='black', s=200, alpha=0.5)
        #plt.savefig(opt_location + 'cluster_plot_{}_{}.jpg'.format(len(words), reduction_method))
        plt.show()
        return get_labels_in_clusters(y_kmeans, words, {})


def concat_similar(similar,set_to_update):
        """  returns union of similar words from similarity function
         Args:
            similar (list) : list of tuples returned from "most_similar" method in word2vec
            set_to_update (set) : contains final set of words to be used for concatenation
        """
        words = [k[0] for k in similar]
        set_to_update.update(set_to_update.union(set(words)))
        return words

def find_similar_keywords(obj,init_words=None,max_limit=100,topn=15):
        """  returns relevant words based on a list of initial words using similarity function in word2vec
        works only with a defaut set of words to start with.
         Args:
            obj (object) of word2vec class
            init_words (list) : list of words to start with, default None, If no words are given we use our default set of skill words as init_words_for_clustering
            max_limit (int) maximum no. of words to consider. The final list will have m = max_limit +- topn total words
            topn (int) : total words to consider for similarity
        """
        if init_words is None:
                temp_list = default_words_for_clustering.copy()
        else:
                temp_list = init_words
        final_set_keywords = set()
        i=0
        while(len(final_set_keywords) <= max_limit):
                final_set_keywords = final_set_keywords.union([temp_list[i]])
                try:
                        topn_similar = obj.model.wv.most_similar(temp_list[i], topn=topn)
                except Exception as e:
                        print("Exception: suggested baseword '{}' not found in the vocab".format(temp_list[i]))
                temp_list.extend(concat_similar(topn_similar,final_set_keywords))
                i+=1
        return list(final_set_keywords)

def defined_color_func(word=None, font_size=None, position=None,
                      orientation=None, font_path=None, random_state=None):
            """Random hue color generation.

            Default coloring method. This picks a random one from a set of defined
            colors with specific hue and lumination percentage.
            Args:
            word, font_size, position, orientation  : ignored.
            random_state : random.Random object or None, (default=None)
                If a random object is given, this is used for generating random numbers.
            """
            #hsl_colors = [d5691f,1a415f,35709e,59d9c4,2e9b89,f5ab47,f58d47,31c7ff,fe5033]
            hsl_colors = [(24,75,48),(206,57,24),(206,50,41),(170,63,60),(170,54,39),
                          (34,90,62),(24,90,62),(196,100,60),(9,99,60)]
            random_num = random.randint(0,len(hsl_colors)-1)
            hue= hsl_colors[random_num][0]
            saturation=hsl_colors[random_num][1]
            lightness = hsl_colors[random_num][2]
            #return "hsl(%d, 80%%, 50%%)" % random_state.randint(0, 255)
            return str("hsl("+str(hue)+", "+str(saturation)+"%,"+str(lightness)+"%)")



def get_tfidf_scores(keywords, segmented=False, bigrams=True, tfidf_source="../data/features_tfidf/", dc={}):
        """
        Load the file containing tfidf scores from all text in the corpus
        Args:
                keywords: (list)
                segmented: (bool) True if partitioned data with respect to profiles, else False for all text corpus
                bigrams : (bool) True if the model contains bigram vocabulary
                profile : (str) if segmented, which profile do the keywords belong to.
        :returns a dictionary with dataframe of TFIDF scores for the keywords.
        """
        #Change the source if it is a bigram model. 
        if(bigrams):
                tfidf_source = "../data/features_tfidf_bigrams/"

        if (segmented):
                fname = [a for a in os.listdir(tfidf_source) if a.startswith("features_tfidf_")]
        else:
                fname = ["features_tfidf_all text.csv"]
        for files in fname:
                tfidf_df = pd.read_csv(tfidf_source + files, names=['features', 'tfidf_scores'])
                skill = (files.split("_")[-1]).replace(".csv", "")
                dc[skill] = tfidf_df[tfidf_df['features'].isin(keywords)]
        return dc


def create_wordclouds(buzzwords,use_tfidf=False, file_to_write="../plots/wordclouds.jpg"):
        """
        Create wordclouds for the keywords supplied, if use_tfidf is set to true, the tfidf_scores are used as frequencies
        Args:
        buzzwords: (list) of keywords supplied
        use_tfidf: (bool) True if tfidf scores are used as the word frequency (size)
        file_to_write: (str) path where the file is to be written
        :return: None
        """

        if use_tfidf:
                scores = 0 # use TFIDF scores
        else:
                scores = [(i,random.random()) for i in buzzwords]
        #wc = WordCloud(width=320, height=250, color_func=defined_color_func, background_color='white',
        #          scale=2, prefer_horizontal=0.7).generate_from_frequencies(scores)
        #wc.to_file(file_to_write)
        #print("creating wordcloud")
        #plt.show()

        ##### Code without using wordcloud package
        plt.figure(figsize=(9, 5), dpi=320)
        hsl_cols = ['#d5691f', '#1a416f', '#35709e', '#59d9c4', '#2e9b89', '#f5ab47', '#f58d47', '#f31c7f', '#fe5033']
        for word, score in scores:
                color = random.choice(hsl_cols)
                x = random.random()
                y = random.random()
                plt.text(y, x, word, fontweight='bold', color=color)
        #adjusting limits on x and y axis to prevent words from overflowing out of the frame
        plt.xlim(0.0,1.16)
        plt.ylim(0.0,1.05)
        plt.savefig(file_to_write)
        plt.show()

	
def cluster_texts(obj, n_clusters=3):
        """ Transform texts to Word2Vec coordinates and cluster texts using K-Means
        Args:
        obj (object) of word2vec class
        n_clusters (int) default number of cluster to create
        """
        X = self.model[self.model.wv.vocab]
        km_model = KMeans(n_clusters=n_clusters)
        km_model.fit(X)
        clustering = collections.defaultdict(list)
        for idx, label in enumerate(km_model.labels_):
            clustering[label].append(idx)
        return clustering

